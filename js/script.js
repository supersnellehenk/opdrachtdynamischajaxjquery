// AJAX

let brandArray = [];
let modelArray = [];
let versionArray = [];

$(document).ready(function () {
  $.get(`PHP/scripts/getCars.php?mode=init`, function (data) {
    let brandOptions = `<option disabled selected>Selecteer een merk</option>`;
    data = JSON.parse(data);
    data.forEach(i => {
      brandOptions += `<option value="${i}">${i}</option>`;
    });
    $('#brandSelect').html(brandOptions);
    $('#modelSelect').html(`<option disabled selected>Selecteer een model</option>`);
    $('#versionSelect').html(`<option disabled selected>Selecteer een uitvoering</option>`);
  });
});

$('#brandSelect').change(function () {
  const brand = $('#brandSelect').val();
  $.get(`PHP/scripts/getCars.php?mode=models&brand=${brand}`, function (data) {
    let modelOptions = `<option disabled selected>Selecteer een model</option>`;
    data = JSON.parse(data);
    data.forEach(i => {
      modelOptions += `<option value="${i}">${i}</option>`;
    });
    $('#modelSelect').html(modelOptions);
    $('#versionSelect').html(`<option disabled selected>Selecteer een uitvoering</option>`);
    $('#car-image').attr('src', '');
  });
});

$('#modelSelect').change(function () {
  const brand = $('#brandSelect').val();
  const model = $('#modelSelect').val();
  $.get(`PHP/scripts/getCars.php?mode=versions&model=${model}&brand=${brand}`, function (data) {
    let versionOptions = `<option disabled selected>Selecteer een uitvoering</option>`;
    data = JSON.parse(data);
    data.forEach(i => {
      versionOptions += `<option value="${i}">${i}</option>`;
    });
    $('#versionSelect').html(versionOptions);
    $('#car-image').attr('src', '');
  });
});

$('#versionSelect').change(function () {
  const brand = $('#brandSelect').val();
  const model = $('#modelSelect').val();
  const version = $('#versionSelect').val();
  $.get(`PHP/scripts/getCars.php?mode=image&version=${version}&model=${model}&brand=${brand}`, function (data) {
    $('#car-image').attr('src', data);
  });
});